const SES = require('aws-sdk/clients/ses');

const ses = new SES({
    accessKeyId: process.env.S3_ACCESS_KEY_ID,
    secretAccessKey: process.env.S3_SECRET_ACCESS_KEY,
    apiVersion: process.env.S3_API_VERSION,
    region: process.env.S3_SES_REGION,
});


async function sendEmail(email) {

    if (!email) {
        return;
    }

    const params = {
        Destination: {
            ToAddresses: email.email
        },
        Message: {
            Body: {
                Html: {
                    Charset: "UTF-8",
                    Data: email.body,
                },
            },
            Subject: {
                Charset: "UTF-8",
                Data: email.subject
            }
        },
        ReplyToAddresses: [],

        Source: process.env.S3_MAIL_SENDER,

    };
    try {
        await  sendEmailToSes(params)
    } catch (error) {
        throw error;
    }

}

function sendEmailToSes(params) {
    //exec call to SES as a promise to wait for response
    return ses.sendEmail(params).promise()
}


module.exports = {
    sendEmail: sendEmail
};
