var request = require('request');
const Message = require('../models').Message;
module.exports =  class Eas{
   
    constructor(message,capcode){
        this.spitMessage = '';
        this.rawMessage = message;
        this.message = message;
        this.response = {};
        this.response.capcode = capcode;
        this.score = {
            validated: 0,
            shortname: 0,
            agencyType: 0,
        }
        this.init();

    }

    init(){
        this.validateMessage();
        this.trimMessage();
        this.splitMessage();
        this.setShortname();
        this.checkAgencyEventNumber();
  
        this.response.message = this.message;
        this.response.score = this.score;
     
 

        console.log(this.response)
        
    }     

    async validateMessage(){
        //@@ - Emergency page
        if(this.message.substring(0, 7) == '@@ALERT'){
            this.response.sourceType = 'Emergency';
            console.log('ALERT');
            
           // this.getEVdata();
            //await this.getEVdata();
           // checkAgencyEventNumber(spitMessage)
           // checkSesUnitCode(spitMessage)
           // message = message.slice(2);
        }
        //Hb - Non Emergency Message
        else if(this.message.substring(0, 2) == 'Hb'){
            this.response.sourceType = 'Non Emergency';
            console.log('Hb');
           // message.slice(2);
        }
        //QD -Admin Message
        else if(this.message.substring(0, 2) == 'QD'){
            this.response.sourceType = 'Admin';
            console.log('QD');
           // message.slice(2);
        }
        
        else{
            return;
        }

        this.score.validated = 1

    }

    trimMessage(){
        this.message = this.message.slice(2);
    }
    
    splitMessage(){
        this.spitMessage = this.message.split(" ");
    }
    

    setShortname(){
        let pattern = /\[.*?\]/
        let last = this.spitMessage[this.spitMessage.length-1];
        if(last.match(pattern)){
            last = last.replace("[","")
            last = last.replace("]","")
            this.response.shortname = last
            this.score.shortname = 1
        }
    }

    checkAgencyEventNumber(){
        let twoback = this.spitMessage[this.spitMessage.length-2];
        console.log(this.spitMessage[1])
        var sespattern = /S\d{6}/
        var firepattern = /F\d{6}/
        if(twoback.match(firepattern)){
            //event agency
            this.response.agencyType = 'FIRE'; // event agency
            this.score.agencyType = 1

            //event code
            this.response.eventcode = this.spitMessage[2]
            this.score.eventcode = 1

            //event number
            this.response.eventId = twoback
            this.response.eventIdNumber = twoback.slice(1);
            this.getEVdata(this.response.eventIdNumber)
       
        }
        //SES - EMERGENCY @@ALERT MESSAGE
        else if(this.spitMessage[1].match(sespattern)){
            //event agency
            this.response.agencyType = 'SES';
            this.score.agencyType = 1
            
            //event unit
            this.response.eventUnit = this.spitMessage[2]
            this.score.eventUnit = 1

            //event code
            this.response.eventcode = this.spitMessage[4]
            this.score.eventcode = 1


            //event number
            this.response.eventId = this.spitMessage[1]
            this.response.eventIdNumber = this.spitMessage[1].slice(1);
            this.getEVdata(this.response.eventIdNumber)

        }
         //SES - NON EMERGENCY MESSAGE
         else if(this.spitMessage[0].match(sespattern)){
            //event agency
            this.response.agencyType = 'SES';
            this.score.agencyType = 1
            
            //event unit
            this.response.eventUnit = this.spitMessage[1]
            this.score.eventUnit = 1

             //event code
             this.response.eventcode = this.getSESeventType()[1]
             this.score.eventcode = 1

             //event detail
             this.response.eventDetail = this.getSESeventType()[2]
             this.score.eventDetail = 1

            //event number
            this.response.eventId = this.spitMessage[0]
            this.response.eventIdNumber = this.spitMessage[0].slice(1);
            this.getEVdata(this.response.eventIdNumber)
            
        }
      
    }

    // function getLocation(){
    //    

        
    // }

    async getEVdata(id){
        const delay = ms => new Promise(res => setTimeout(res, ms));
        if(!id){
            console.log('no id to process')
            return;
        }

        console.log('waiting 55 sec');
        await delay(55000); 

        console.log('getEVdata')
        console.log(id)
        return new Promise((resolve, reject) => {
            let checkId = String(id);

            request.get({
                url: 'http://emergency.vic.gov.au/public/osom-geojson.json',
                gzip: true
            }, (err, response, body)   =>    {
          
              let data = JSON.parse(body); //item.geometry
           
              let event = data.features.filter(item=> {
                if(!item.properties.eventId){
                    return;
                }

                let checkEventId = String(item.properties.eventId);
            
                if(checkEventId == checkId){
             
                    Message.findOne(
                        { where: { message:  this.message }})
                    .then(async (msgItem)=>{
                    
                        if(msgItem){
                            Message.update(
                            {geom: item.geometry, noLocation: null},
                            {where: {  message:  this.message }}
                        )
                        .then(result =>{
                        
                           return item;
                            }
                          )
                          .catch(err =>
                            console.log(err)
                          )
                        
                        
                        
                        }
                        else{ console.log('eas not updated')}
                    })
                    .catch((error) => {
                        console.log(error);
                    });
                 
                }
              })

             // console.log(event)



            // if(event[0]){  
            //     event = event[0];
            //     let e = {
            //         id: event.properties.eventId,
            //         geom: event.geometry
            //     }
            //     console.log(e)
            // }
            // else{
            //     console.log('no match')
            // }
          
                
                
              
            })

        })
    }

    getSESeventType(){
       let message = this.message.split(' - ');
       return message;
    }

}