'use strict';
const path = require('path');
var rawPath = path.join(__dirname, '../public/images/raw/');
var exportPath = path.join(__dirname, '../public/images/export/');
const imagemin = require('imagemin');
const imageminJpegtran = require('imagemin-jpegtran');
const imageminPngquant = require('imagemin-pngquant');
var Raven = require('raven');


module.exports = {
    resizeImage,
    noResize,
    optimizeImage
}


function resizeImage(file) {


    return new Promise((resolve, reject) => {
        var Jimp = require('jimp');
        Jimp.read(file.path)
            .then(image => {
                var sizes = [
                    //ios list
                    {size: 60, name: '-list'},
                    {size: 120, name: '-list@2x'},
                    {size: 180, name: '-list@3x'},
                    //ios profile
                    {size: 130, name: '-profile'},
                    {size: 260, name: '-profile@2x'},
                    {size: 390, name: '-profile@3x'}

                ];

                var files = [];

                var fileType = file.mimetype;
                fileType = fileType.split("/");
                fileType = '.' + fileType[1]
                // Do stuff with the image.
                // image.resize(50, 50) // resize
                //lower the quality by 90%
                // .quality(10)
                // .write(exportPath+file.filename);
                // .getBuffer(type.mime, (err, buffer)=>{
                //console.log(image);
                for (var i = 0; i < sizes.length; i++) {
                    let resize = {
                        path: exportPath + file.filename + i + fileType,
                        filename: file.filename + i + fileType
                    }
                    files.push(resize);
                    image.clone().resize(sizes[i], Jimp.AUTO)
                        .write(exportPath + file.filename + sizes[i].name + fileType);

                }

                resolve(files);
            })
            .catch(err => {
                // Handle an exception.
                console.log(err)

            });

    })

}

function noResize(file) {
    return new Promise((resolve, reject) => {

        var fileType = file.mimetype;
        fileType = fileType.split("/");
        fileType = '.' + fileType[1]

        let resize = {
            path: rawPath + file.filename,
            filename: file.filename + fileType
        }
        var files = [];
        files.push(resize);
        resolve(files);

    });
}

function optimizeImage(file, quantity = [0.65, 0.85], maxWidth = 1920, maxHeight = 1080) {
    return new Promise((resolve, reject) => {
        const Jimp = require('jimp');

        Jimp.read(file.path)
            .then(image => {
                if (image.bitmap.width > maxWidth || image.bitmap.height > maxHeight) {
                    if (image.bitmap.width < image.bitmap.height) {
                        /*
                        * Vertical image
                        */
                        image.resize(Jimp.AUTO, maxHeight)
                            .write(file.path);
                    } else {
                        /*
                        * Horizontal image
                        */
                        image.resize(maxWidth, Jimp.AUTO)
                            .write(file.path);
                    }
                }
            })
            .then(() => {
                /*
                * Optimize Image
                */
                imagemin([file.path], exportPath, {
                    plugins: [
                        imageminJpegtran({quality: quantity}),
                        imageminPngquant({quality: quantity})
                    ]
                })
                    .then(filesOptimize => {
                        let resize = {
                            path: filesOptimize[0].path,
                            filename: file.filename
                        };
                        const files = [];
                        files.push(resize);
                        resolve(files);
                    })
                    .catch(err => {
                        // Handle an exception.
                        if (process.env.NODE_ENV === 'production') {
                            /*
                            * Sentry
                            */
                            Raven.captureMessage(`Error optimize images`, {
                                extra: {
                                    error: err
                                }
                            });
                        }
                        console.log(`========Error optimize images======`);
                        console.log(err)
                        resolve(false);
                    });
            })
            .catch(err => {
                if (process.env.NODE_ENV === 'production') {
                    /*
                    * Sentry
                    */
                    Raven.captureMessage(`Error optimize images Jimp read`, {
                        extra: {
                            error: err
                        }
                    });
                }
                // Handle an exception.
                console.log(`========Error optimize images Jimp read======`);
                console.log(err.message)
                resolve(false);
            });
    });

}

// ios
// profile size: 130 x130 | 260 x 260 | 390 x 390
// list size: 60 x60 | 120 x 120 | 180 X 180

