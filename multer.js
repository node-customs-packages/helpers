var multer = require('multer');
const path = require('path');

module.exports = {
    detectFormDataUploadFile
}


function detectFormDataUploadFile() {
    let storage = multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, path.join(__dirname, '../public/images/raw'))
        },
        filename: (req, file, cb) => {
            let fileType = file.mimetype;
            fileType = fileType.split("/");
            fileType = '.' + fileType[1]
            let fileName = file.originalname;
            fileName = fileName.split(".");
            cb(null, fileName[0] + '_' + Date.now() + fileType)
        }
    });
    return multer({storage: storage});
}

