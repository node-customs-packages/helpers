const _ = require('lodash');
const Validator = require('validatorjs');
const GlobalConstants = require('../../server/constants/globalConstants');

module.exports = (data, rules, language = GlobalConstants.Languages.DEFAULT_LANGUAGE_CODE, isObject = false) => {
    return new Promise((resolve, reject) => {
        Validator.useLang(language);
        let validator = new Validator(data, rules);
        validator.passes(() => resolve(false));

        validator.fails(() => {
            let errors = validator.errors.all();
            if (isObject) {
                return resolve(errors);
            }
            let messageError = '';
            for (let param in errors) {
                messageError = errors[param][0];
                break;
            }
            return resolve(messageError);
        });
    })
}
