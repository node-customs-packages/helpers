const CronJob = require('cron').CronJob;
const Watchdog = require('../models').Watchdog;
const Message = require('../models').Message;
const notificationController = require('../modules/notification-management/controller');
module.exports =  () =>{


        new CronJob('*/15 * * * * *', function() {
            runWatchdog();
        }, null, true, 'Australia/Melbourne');



       runWatchdog = async function(){
        //get a list of items beings watched
        let dogs = await Watchdog.listDogs();
        //check each item
        dogs.map(dog=>dogCheck(dog))
       
       }
       
       dogCheck = function(item){
        let check = item.get({plain: true})

        let now = new Date()
        let lastChecked = new Date(check.checkedIn)
        let diffMs = (now - lastChecked); 
        var minutes = Math.floor((diffMs/1000)/60);
  

        if(minutes > item.period){
            // triger alarm
            if(item.alertSent != true){
                triggerAlarm(item.name)
            }
        }
        else{
            // reset the alarm
            if(item.alertSent != false){
                resetAlarm(item.name)
            }
        }
         
       }

        triggerAlarm = function(name){
            
            let payload = {
                name: name,
                status: 'Unhealthy',
                alertSent: true,
                alertType: 'alert',
                alertTypeLabel: 'Alert',
                message: name.toUpperCase() + ' has triggered and is now UNHEALTHY and in ALARM. @@ACTION REQUIRED.'
            }
            tiggerAlert(payload)
            Watchdog.updateAlarm(payload)
        }

        resetAlarm = function(name){
            let payload = {
                name: name,
                status: 'Healthy',
                alertSent: false,
                alertType: 'message',
                alertTypeLabel: 'Message Only',
                message: name.toUpperCase() + ' has returned HEALTHY the alarm is now reset. Enjoy your day. 🐕🐕'
            }
            tiggerAlert(payload)
            Watchdog.updateAlarm(payload)
        }

        tiggerAlert = function(payload){
          
            let internalMessage = {
                noLocation: true,
                sourceId: 1,
                teamId: 1,
                message: payload.message,
                type: payload.alertType,
                typeLabel: payload.alertTypeLabel,
                issued: new Date()
            }

            return Message.create(internalMessage)
            .then((item) => {
                if(item) {
                    //send notificaitons
                    notificationController.sendNotificaitonForMesssageInternal(item.id).catch((err) => {console.log(err)});
                }  
            })
            .catch((error) => {
                console.log(error);
            });


        }

}