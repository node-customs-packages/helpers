const path = require('path');
const apn = require('apn');
const FCM = require('fcm-push');
const Device = require('../models').Device;


//Set FCM Details
const serverKey = 'AAAAxEhJa5c:APA91bEnyEe3VpxbT4iBCgrqcAo2XhbKbmR4mf-MN578x6DjdcvwjIzNmgGoz5nATUWsjz7PGrHNGVt5qFgTSAK5osJeZ9vn60uhLozWpSqSqK2qMFlPJTi8o-EH95vB0lhnfK152fLN';
const fcm = new FCM(serverKey);

//Options for APN
var options = {
    token: {
        key: path.join(__dirname, './../config/AuthKey_Y4HHHR68N8.p8'),
        keyId: "Y4HHHR68N8",
        teamId: "25G6YSU5HB"
    },
    production: true
};
// Create PROD Instance of provider  
const apnProvider = new apn.Provider(options);
// Create SANDBOX Instance of provider
options.production = false;
const sandboxApnProvider = new apn.Provider(options);


module.exports = {
    fcmSend,
    apnSend
}

function fcmSend(message) {
    fcm.send(message)
        .then(function (result) {
            return
        })
        .catch(function (error) {
            //deregister device
            return deregisterTokenAOS(error, message.to);
        })
}

function apnSend(apnPayload) {

    try {
        let deviceToken = apnPayload.deviceTokens;
        let deviceTokenSandbox = apnPayload.deviceTokensSandbox;

        var note = new apn.Notification();
        //Data at which the notification is no longer valid
        note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
        //Badge number to display
        note.badge = 1;
        //PNS Sound
        note.sound = apnPayload.sound;
        //Message Title and Body
        note.alert = apnPayload.alert;
        //Actionable Notifications Category
        note.category = apnPayload.category;
        //General Payload
        note.payload = apnPayload.payload;
        //App Topic - bundle ID  
        note.topic = process.env.APN_TOPIC;

        if (typeof apnPayload.contentAvailable != 'undefined') {
            note.contentAvailable = apnPayload.contentAvailable;
        }

        if (typeof apnPayload.mutableContent != 'undefined') {
            note.mutableContent = apnPayload.mutableContent;
        }

        //send distribution notificaitons
        if (deviceToken) {
            apnDistribution(note, deviceToken)
        }
        //send sandbox notificaitons
        if (deviceTokenSandbox) {
            apnSandbox(note, deviceTokenSandbox)
        }
        // //Added to allow for local device tone
        // if(apnPayload.contentAvailable){
        //     note['content-available'] =  1
        // }
    } catch (error) {
        // console.log(`=====PNS Fail apnSend===== : ${typeof error === 'string' ? error : JSON.stringify(error)}`);
    }

}

function apnDistribution(note, deviceToken) {
    apnProvider.send(note, deviceToken)
        .then((result) => {

            let rs = false;
            if (result.sent.length > 0 && result.failed.length < 1) {
                rs = true;
            }

            //check for fails and deregister pns on device
            result.failed.forEach(payload => {
                return deregisterTokenIOS(payload);
            });

            //PushNotificationLog.create(log);
            return result;

        })
        .catch((error) => deregisterTokenIOS(error));
}


function apnSandbox(note, deviceToken) {
    sandboxApnProvider.send(note, deviceToken)
        .then((result) => {

            let rs = false;
            if (result.sent.length > 0 && result.failed.length < 1) {
                rs = true;
            }

            //check for fails and deregister pns on device
            result.failed.forEach(payload => {
                // //deregister device
                return deregisterTokenIOS(payload);
            });

            //PushNotificationLog.create(log);
            return result;

        })
        .catch((error) => deregisterTokenIOS(error));
}

function deregisterTokenAOS(error, deviceToken) {
    if (error == 'NotRegistered') {
        return Device.deregisterToken(deviceToken);
    }
    return;
}

function deregisterTokenIOS(error) {
    if (error.response.reason == 'BadDeviceToken' || error.response.reason == 'Unregistered') {
        /*
        * Disable device push fail
        */
        return Device.deregisterToken(error.device);
    }
    return;
}
