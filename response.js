const _ = require('lodash');
const paginate = require('express-paginate');
const GlobalConstants = require('../../server/constants/globalConstants');

module.exports = (req, res, data = {}, code = GlobalConstants.HeaderStatus.CODE_200) => {
    const today = new Date();
    const gmt = today.toUTCString();
    let result = {
        name: `${GlobalConstants.PROJECT_NAME}`,
        code: code,
        messages: 'Successful',
        created: gmt,
        data: data
    };
    if (code === GlobalConstants.HeaderStatus.CODE_200) {
        /*
        * Check Pagination
        */
        if (_.has(data, 'count') && _.has(data, 'rows')) {
            const itemCount = data.count;
            const pageCount = Math.ceil(itemCount / req.query.limit);
            result.data = data.rows;
            result.pagination = {
                current_page: parseInt(req.query.page),
                last_page: pageCount,
                per_page: parseInt(req.query.limit),
                total_pages: pageCount,
                total_rows: itemCount,
                pages: paginate.getArrayPages(req)(3, pageCount, req.query.page)
            };
        }
        if (_.isString(data)) {
            result.messages = data;
            delete result.data;
            delete result.pagination;
        } else if (data.data && data.message) {
            result.messages = data.message;
            result.data = data.data;
        }
        /*
        * Detect Multi languages
        */
        if(typeof result.data != "undefined") {
            let dataMulti = JSON.parse(JSON.stringify(result.data));
            result.data = detectMultiLanguages(dataMulti, req.language);
        }
    } else {
        result.messages = data.message;
        if (_.isString(data)) {
            result.messages = data;
        }
        if (_.has(data, 'errors')) {
            result.messages = data.errors[0].message;
        }

        if (process.env.NODE_ENV == 'development') {
            result.data = data;
        } else {
            delete result.data;
            delete result.pagination;
        }

    }


    return res.status(code).send(result);
};

function detectMultiLanguages(data, language = GlobalConstants.Languages.DEFAULT_LANGUAGE_CODE) {
    let ignoreField = ['id', 'lang', 'createdAt', 'updatedAt'];
    if (_.isArray(data) && data.length) {
        for (let item of data) {
            detectMultiLanguages(item, language);
        }
    }
    if (_.isObject(data) && !_.isEmpty(data)) {
        for (let param in data) {
            if (_.isArray(data[param]) && data.length) {
                /*
                * Check child is array
                */
                for (let item of data[param]) {
                    detectMultiLanguages(item, language);
                }
            }

            if (_.isObject(data[param]) && !_.isEmpty(data)) {
                /*
                * Check child is array
                */
                detectMultiLanguages(data[param], language);
            }
            if (param === 'langs' && data.langs.length) {
                /*
                * Get object language with language require
                */
                const langObject = data.langs.filter(lang => lang.lang === language);
                if (!langObject.length) {
                    delete data.langs;
                    continue;
                }
                for (let field in langObject[0]) {
                    if (typeof data[field] != "undefined" && !ignoreField.includes(field)) {
                        data[field] = langObject[0][field];
                    }
                }
                delete data.langs;
            }
        }
    }
    return data;
}
