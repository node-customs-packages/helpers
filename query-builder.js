let _ = require('lodash');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const GlobalConst = require('../../server/constants/globalConstants');

module.exports = (query) => {
    let result = {};
    let dataParams = Object.assign({}, query);
    if (!_.isEmpty(dataParams)) {
        /*
        * Check Order
        */
        if (_.has(dataParams, 'order_by') && _.has(dataParams, 'order_type')) {
            result.order = [];
            let order = dataParams.order_by.split(",");
            order.push(dataParams.order_type);
            result.order.push(order);
            delete dataParams.order_by;
            delete dataParams.order_type;
        }
        /*
        * Check Attributes
        */
        if (_.has(dataParams, 'attributes')) {
            let attributes = dataParams.attributes.split(",");
            result.attributes = attributes;
            delete dataParams.attributes;
        }
        /*
        * Check limit and offset
        */
        if (_.has(dataParams, 'limit') && _.has(dataParams, 'page')) {
            result.limit = dataParams.limit;
            result.offset = dataParams.limit * (dataParams.page - 1);
            delete dataParams.limit;
            delete dataParams.page;
        }
        /*
        * Build Where query
        */
        let where = {};
        for (let field in dataParams) {

            if (typeof field == 'string' && field.includes('|')) {
                /*
                * Check Field
                */
                let operator = field.split('|');

                if (operator[1] == 'like') {
                    let column = operator[0];
                    if (operator[0].includes('.')) {
                        /*
                        * Check filter by model
                        */
                        let {column: column, isJsonB: isJsonB} = generateColumnField(operator[0]);
                        if (isJsonB) {
                            /*
                            * Filter with JSONB column
                            */
                            where[Op.and] = typeof where[Op.and] == 'undefined' ? {} : where[Op.and];
                            where[Op.and][column] = {
                                [Op.iLike]: `%${dataParams[field].toLowerCase()}%`
                            };
                        } else {
                            /*
                            * Filter with Model
                            */
                            where[Op.and] = Sequelize.where(Sequelize.fn('lower', Sequelize.col(column)), 'LIKE', `%${dataParams[field].toLowerCase()}%`);
                        }

                    } else {
                        where[Op.and] = Sequelize.where(Sequelize.fn('lower', Sequelize.col(column)), 'LIKE', `%${dataParams[field].toLowerCase()}%`);
                    }
                } else {
                    let value = dataParams[field];
                    let column = operator[0];
                    if (operator[0].includes('.')) {
                        /*
                        * Check filter by model
                        */
                        let {column: column, isJsonB: isJsonB} = generateColumnField(operator[0]);
                        if (isJsonB) {
                            if (typeof where[column] == 'undefined') {
                                where[column] = {
                                    [Op[operator[1]]]: value
                                }
                            } else {
                                where[column][Op[operator[1]]] = value;
                            }
                        } else {
                            /*
                            * TODO: Not support with model and Operators
                            */
                            where[Op.and] = Sequelize.where(Sequelize.col(column), Op[operator[1]], value);
                        }
                    } else {
                        if (typeof where[column] == 'undefined') {
                            where[column] = {
                                [Op[operator[1]]]: value
                            }
                        } else {
                            where[column][Op[operator[1]]] = value;
                        }

                    }
                }
            } else if (typeof field == 'string' && !field.includes('|') && field.includes('.')) {
                /*
                * Check filter with JSONB column
                */
                let {column: column, isJsonB: isJsonB} = generateColumnField(field);
                if (isJsonB) {
                    where[Op.and] = {};
                    where[Op.and][field] = dataParams[field];
                } else {
                    where[Op.and] = Sequelize.where(Sequelize.col(column), dataParams[field]);
                }

            } else {
                where[field] = dataParams[field];
            }
        }
        result.where = where;
    }
    return result;
}

function generateColumnField(field) {
    let model = field.split('.');
    let column = field;
    if (GlobalConst.JSONB_COLUMNS.includes(model[0])) {
        return {column: column, isJsonB: true};
    }
    if (model.length > 2) {
        column = '';
        for (let i in model) {
            if ((parseInt(i) + 1) < model.length) {
                column += model[i] + '->';
            }

            if ((parseInt(i) + 1) == model.length) {
                column = column.substring(0, column.length - 2);
                column += '.' + model[i];
            }

        }
    }
    return {column: column, isJsonB: false};
}
