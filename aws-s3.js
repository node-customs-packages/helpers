const fs = require('fs');
const S3 = require('aws-sdk/clients/s3');
const ThumbnailGenerator = require('video-thumbnail-generator').default;
const path = require('path');
const a2mImages = require('./jimp')
const s3 = new S3({
    accessKeyId: process.env.S3_ACCESS_KEY_ID,
    secretAccessKey: process.env.S3_SECRET_ACCESS_KEY,
    apiVersion: process.env.S3_API_VERSION,
    region: process.env.S3_REGION,
    s3ForcePathStyle: true
});
const Raven = require('raven');

module.exports = {
    uploadImageSingle,
    uploadImageArray,
    uploadVideoSingle,
    isImage,
    isVideo,
    createThumbnailVideo,
    uploadFileSingle,
    uploadFilesArray
};

async function uploadVideoSingle(file, folder) {
    if (!isVideo(file)) {
        return false;
    }
    //var processedVideo = a2mImages.noResize(file);
    return await setupAndSendToS3(file, file, folder);
}

async function uploadImageArray(files, folder) {
    let newImages = [];
    //need to use this type of loop to process in sequence and await response
    for (const file of files) {
        let image = await uploadImageSingle(file, folder);
        if (!image) {
            break;
        }
        newImages.push(image);
    }

    return newImages;
}

async function uploadImageSingle(file, folder) {
    return new Promise(async (resolve, reject) => {
        setTimeout(async () => {
            let processedImages = await a2mImages.optimizeImage(file);
            if (!processedImages) {
                /*
                * Optimize image false
                */
                return resolve(false);
            }

            let name;
            try {
                for (let i = 0; i < processedImages.length; i++) {
                    name = await setupAndSendToS3(file, processedImages[i], folder);
                }
            } catch (error) {
                throw error;
            }

            return resolve(name);
        }, 3000);
    })
}

async function uploadFilesArray(files, folder) {
    let newFiles = [];
    //need to use this type of loop to process in sequence and await response
    for (const file of files) {
        let fil = await uploadFileSingle(file, folder);
        if (!fil) {
            break;
        }
        newFiles.push(fil);
    }

    return newFiles;
}

async function uploadFileSingle(file, folder) {
    return await setupAndSendToS3(file, file, folder);
}

function setupAndSendToS3(uploadFile, resize, folder) {
    return new Promise(async (resolve, reject) => {
        let fileName = resize.filename; //uploadFile.filename + '.' + fileType[1];
        const params = {
            Bucket: process.env.S3_IMAGE_BUCKET,
            Key: folder + fileName,
            Body: fs.createReadStream(resize.path), //(uploadFile.path),
            ContentType: uploadFile.mimetype, // this will allow it to be viewed inline
            ACL: 'public-read' // will allow it to be accessable to view
        };

        await uploadToS3(params)
            .then(data => {
                //delete the local file
                const path = require('path');
                const rawPath = path.join(__dirname, '../public/images/raw/');
                const exportPath = path.join(__dirname, '../public/images/export/');
                if (fs.existsSync(rawPath + fileName)) {
                    fs.unlinkSync(rawPath + fileName) //(uploadFile.path);
                }
                if (fs.existsSync(exportPath + fileName)) {
                    fs.unlinkSync(exportPath + fileName) //(uploadFile.path);
                }
                //set the filename to return
                return resolve(data.Key);

            })
            .catch(async(error) => {
                if (process.env.NODE_ENV === 'production') {
                    /*
                    * Sentry
                    */
                    Raven.captureMessage(`Error upload file to S3`, {
                        extra: {
                            error: error.message,
                            data: params
                        }
                    });
                }
                console.log(`=====Error upload file to S3======: ${error.message}`);
                console.log(error);
                resolve(error);
            });
    })
}

function uploadToS3(params) {
    //exec call to S3 as a promise to wait for response
    return s3.upload(params).promise();
}


function getExtension(mimetype) {
    const parts = mimetype.split('/');
    return parts[parts.length - 1];
}

function isImage(file) {
    const ext = getExtension(file.mimetype);
    const validImageTypes = ['jpg', 'jpeg', 'png'];
    if (!validImageTypes.includes(ext.toLowerCase())) {
        return false;
    }
    return true;
}

function isVideo(file) {
    const ext = getExtension(file.mimetype);
    const validVideoTypes = ['mp4'];
    if (!validVideoTypes.includes(ext.toLowerCase())) {
        return false;
    }
    return true;
}

function createThumbnailVideo(file, folder) {
    return new Promise(async (resolve, reject) => {
        const tg = new ThumbnailGenerator({
            sourcePath: file.path,
            thumbnailPath: path.join(__dirname, '../public/images/raw'),
        });
        return tg.generateOneByPercentCb(90, async (err, result) => {
            if (err != null) {
                console.log(err.message);
                return resolve(false);
            }
            const fileUpload = {
                mimetype: 'image/png',
                filename: result,
                path: path.join(__dirname, `../public/images/raw/${result}`)
            };
            const name = await setupAndSendToS3(fileUpload, fileUpload, folder);
            return resolve(name);
        });
    });
}
